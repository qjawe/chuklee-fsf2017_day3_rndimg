//Load the libs
var path = require("path");
var express = require("express");

//Create an instance of the app
var app = express();

var message = [ "one", "two", "three", "four", "five" ]
var msgIdx = 0;

//Load the static resources from the following directory
app.use(express.static(path.join(__dirname, "public")));

app.use("/picture", function(req, res) {

    res.status(200); //HTTP status code
    res.type("text/html"); //MIME type - representation
    res.send("<h1>" + message[msgIdx] + "</h1>");

    msgIdx = (msgIdx + 1) % message.length;

});

app.use("/picture/random", function(req, res) {
    var idx = Math.round(Math.random() * message.length) % message.length;
    res.status(200); //HTTP status code
    res.type("text/html"); //MIME type - representation
    res.send("<h1>" + message[idx] + "</h1>");

})

app.use(function(req, res) {
    res.status(404);
    res.send("error file not found");
});


//Define the port that our app is going to listen to
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

//Start the server
app.listen(app.get("port"), function() {
    console.log("Application started at %s on %d"
            , new Date(), app.get("port"));
});